# -*- coding: utf-8 -*-

__author__ = "Jason Smith"
__credits__ = ["Jason Smith"]
__email__ = "jasons2@cisco.com"

"""
Get the OAuth Token from Cisco
"""

import json
from datetime import datetime
from os import path
from decouple import config
import requests

class CiscoSimpleOAuthToken:

    def __init__(self, tokenStorage=".token"):
        self.tokenStorage = tokenStorage
        self.token = ""
        self.clientId = config('clientId', default='')
        self.clientSecret = config('clientSecret', default='')
        self.rightnow = datetime.now()
        self.tokenInfo = {"tokenObtained" : self.rightnow, "token": self.token}
        self.tokenAge = 0

    def obtain_token(self):
        # this function checks to see if a token has been received in the last 60 mins
        # If it has, it loads it.  If not it runs post API request to odtain token and
        # returns it to be used in other functions

        if not self.fileExists():
            self.generateToken()
            self.storeToken()
        else:
            self.readToken()
            self.calculateTokenAge()

            if self.tokenAge  > 3600:                self.generateToken()
                self.storeToken()
            else:
                self.token = self.tokenInfo['token']

        return self.token

    def generateToken(self):
        """
        contact API, get token and store it in token
        """
        url = f"https://cloudsso.cisco.com/as/token.oauth2?grant_type=client_credentials&client_id={self.clientId}&client_secret={self.clientSecret}"
        try:
            r = requests.post(url)
            token_dict = json.loads(r.text)
            self.token = (token_dict['access_token'])
        except Exception as e:
            print(e)

    def storeToken(self):
        """
        Store the contents of tokenInfo in the file tokenStorage
        """
        self.tokenInfo = {"tokenObtained" : self.rightnow.strftime("%d-%m-%y:%H:%M:%S"), "token": self.token}
        with open(self.tokenStorage, "w") as outfile:
            json.dump(self.tokenInfo, outfile)

    def readToken(self):
        """
        Read contents of tokenStorage and put it in tokenInfo
        """
        with open (self.tokenStorage, "r") as f:
            self.tokenInfo = json.load(f)

    def calculateTokenAge(self):
        """
        Calculate age of current token and store in tokenAge
        """
        tokenLastReceived = datetime.strptime(self.tokenInfo['tokenObtained'], "%d-%m-%y:%H:%M:%S")
        return self.token

    def generateToken(self):
        """
        contact API, get token and store it in token
        """
        url = f"https://cloudsso.cisco.com/as/token.oauth2?grant_type=client_credentials&client_id={self.clientId}&client_secret={self.clientSecret}"
        try:
            r = requests.post(url)
            token_dict = json.loads(r.text)
            self.token = (token_dict['access_token'])
        except Exception as e:
            print(e)

    def storeToken(self):
        """
        Store the contents of tokenInfo in the file tokenStorage
        """
        self.tokenInfo = {"tokenObtained" : self.rightnow.strftime("%d-%m-%y:%H:%M:%S"), "token": self.token}
        with open(self.tokenStorage, "w") as outfile:
            json.dump(self.tokenInfo, outfile)

    def readToken(self):
        """
        Read contents of tokenStorage and put it in tokenInfo
        """
        with open (self.tokenStorage, "r") as f:
            self.tokenInfo = json.load(f)

    def calculateTokenAge(self):
        """
        Calculate age of current token and store in tokenAge
        """
        tokenLastReceived = datetime.strptime(self.tokenInfo['tokenObtained'], "%d-%m-%y:%H:%M:%S")
        self.tokenAge = abs((self.rightnow - tokenLastReceived).total_seconds())

    def fileExists(self) -> bool:
        return path.isfile(self.tokenStorage)
